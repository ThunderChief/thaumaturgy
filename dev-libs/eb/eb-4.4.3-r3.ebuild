# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C library for accessing electronic books (development files)"
HOMEPAGE="https://web.archive.org/web/20120401145849/http://www.sra.co.jp/people/m-kasahr/eb/"
SRC_URI="ftp://ftp.sra.co.jp/pub/misc/eb/eb-${PV}.tar.bz2" #-> ${MY_PN}-${PV}.tar.bz2"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="ipv6 nls threads"


DEPEND="nls? ( virtual/libintl )
		sys-libs/zlib"	# aur says gcc-libs
# Also, apparently, the official repository has this ebuild - I don't know 
# if I should include nls or not

RDEPEND="${DEPEND}
		nls? ( sys-devel/gettext )"
BDEPEND=""

DOCS=( AUTHORS ChangeLog{,.0,.1,.2} NEWS README )

src_unpack()
{
	unpack ${A}
}

src_configure()
{
	econf \
		$(use_with ipv6) \
		$(use_enable nls) \
		$(use_enable threads pthread) \
		--with-pkgdocdir=/usr/share/doc/${PF}/html
}
