# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="C library for accessing electronic books (development files)"
HOMEPAGE="https://web.archive.org/web/20120401145849/http://www.sra.co.jp/people/m-kasahr/eb/"
MY_PN="eb-library"
SRC_URI="ftp://ftp.sra.co.jp/pub/misc/eb/eb-${PV}.tar.bz2" #-> ${MY_PN}-${PV}.tar.bz2"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

#S="${WORKDIR}/${MY_PN}-${PV}"

DEPEND="dev-lang/perl
		sys-libs/zlib"	# aur says gcc-libs - trying without because I have
						# little idea what I'm doing - I don't think it
						# specifically requires gcc-libs
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack()
{
#	unpack ${MY_PN}-${PV}.tar.bz2
	unpack ${A}
	cd "eb-${PV}"
}

#src_prepare()
#{
#
#}

src_configure()
{
	econf
#	./configure
}

src_compile()
{
	emake || die "Compilation failed!"
}
src_install()
{
	emake DESTDIR="${D}" install
#	install -Dm644 COPYING "$pkgdir/usr/share/licenses/${PN}/COPYING"
}
