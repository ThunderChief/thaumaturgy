# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 #distutils-r1

DESCRIPTION="LukeSmith's KJV and optionally Vulgate and Greek bibles on the command line."
HOMEPAGE="https://lukesmith.xyz/articles/command-line-bibles"
EGIT_REPO_URI="https://gitlab.com/LukeSmithxyz/vul.git"

LICENSE="The Unlicense"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
