# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION=""
HOMEPAGE="https://librewolf-community.gitlab.io/"
SRC_URI="https://gitlab.com/librewolf-community/browser/linux/uploads/9c018ee028e864b0a5581fe440421d99/${PN}-${PV}-${PR}-aarch64.pkg.tar.zst"

LICENSE="MPL GPL LGPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="gtk3 \
	mozilla-common \
	libxt \
	startup-notification \
	mime-types \
	dbus-glib \
	ffmpeg \
	nss \
	ttf-font \
	libpulse"
RDEPEND="${DEPEND}"
BDEPEND="networkmanager \
	libnotify \
	pulseaudio \
	speech-dispatcher \
	hunspell-en_US"


