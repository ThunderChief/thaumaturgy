# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# Based on the video on YouTube by MagellanLinux describing custom ebuilds
# https://www.youtube.com/watch?v=GY0NAAVp5mE

EAPI=7

DESCRIPTION="This package is a Hello World as an ebuild demo"
HOMEPAGE="https://github.com/ErikLetson/ebuildexample"
SRC_URI="https://github.com/ErikLetson/${PN}/archive/${P}.tar.gz"
# Should use ebuild variables
# ${PN}: Package name (just the name itself)
# ${P}: The package name and version - e.g., ebuildexample-0.2
#												^ Name		^ version

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

S="${WORKDIR}/${PN}-${P}"

DEPEND=""
RDEPEND="${DEPEND}" # Runtime dependencies
BDEPEND="" # Build dependencies

src_install() # This is an ebuild function - this can be called by ebuild
{
	echo "This is a change"
	make DESTDIR=${D} install || die "make install failed"
}
