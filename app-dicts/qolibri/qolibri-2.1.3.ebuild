# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="qolibri EPWING dictionary viewer"
HOMEPAGE="https://github.com/ludios/qolibri"
SRC_URI="https://github.com/ludios/qolibri/archive/${PV}.tar.gz -> ${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="sys-libs/zlib
		dev-libs/eb
		dev-qt/qtwidgets
		dev-qt/qtnetwork
		dev-qt/qtmultimedia
		dev-qt/linguist-tools
		dev-qt/qtwebengine"
RDEPEND="${DEPEND}"
BDEPEND="dev-util/cmake"

src_unpack()
{
	unpack ${A}
	cd ${A}
}

src_compile()
{
	cmake -DPROJECT_VERSION=${PV} \
		-DCMAKE_INSTALL_PREFIX=/usr
	emake
}

src_install()
{
	emake DESTDIR="${D}" install
}
